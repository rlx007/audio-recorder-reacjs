import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Button, makeStyles } from "@material-ui/core";
import PauseIcon from "@material-ui/icons/Pause";
import StopIcon from "@material-ui/icons/Stop";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";

const MyContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
`;

const useStyles = makeStyles((theme) => ({
  musicbtn: {
    fontSize: 40,
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

const App = () => {
  const [isRecording, setisRecording] = useState(false);
  const [startRecording, setstartRecording] = useState(false);
  const [stopRecording, setstopRecording] = useState(false);

  const [rec, setrec] = useState(null);
  const [recStream, setrecStream] = useState(null);
  const [audioURL, setaudioURL] = useState(null);
  const [audioctx, setaudioctx] = useState(null);

  const classes = useStyles();
  useEffect(() => {
    var audioCtx = new AudioContext();
    setaudioctx(audioCtx);
  }, []);

  useEffect(() => {
    if (startRecording) {
      navigator.mediaDevices
        .getUserMedia({
          audio: true,
          video: false,
        })
        .then(function (stream) {
          console.log(stream);
          setrecStream(stream);
          console.log(audioctx);
          const input = audioctx.createMediaStreamSource(stream);
          console.log(input);
          setrec(
            new window.Recorder(input, {
              numChannel: 1,
            })
          );
          setisRecording(true);
          console.log("recording started");
        })
        .catch((err) => {
          console.log(err);
          console.log("some error");
        });
    }
  }, [startRecording]);

  useEffect(() => {
    if (startRecording) {
      if (isRecording) {
        rec.record();
      } else {
        rec.stop();
      }
    }
  }, [isRecording]);

  useEffect(() => {
    if (startRecording) {
      if (stopRecording) {
        setisRecording(false);
        setstartRecording(false);
        rec.stop();
        console.log("recStream", recStream);
        recStream.getAudioTracks()[0].stop();
        rec.exportWAV(createDownloadLink);
      }
    }
  }, [stopRecording]);

  const createDownloadLink = (blob) => {
    console.log(blob);
    const url = window.URL.createObjectURL(blob);
    setaudioURL(url);
  };

  useEffect(() => {
    console.log("sa", startRecording);
    console.log("st", stopRecording);
    console.log("is", isRecording);
  }, [startRecording, stopRecording, isRecording]);

  useEffect(() => {
    console.log(audioURL);
  }, [audioURL]);

  return (
    <MyContainer>
      <audio controls={true} src={audioURL} />
      {!startRecording ? (
        <Button>
          <FiberManualRecordIcon
            onClick={() => {
              setstartRecording(true);
            }}
            className={classes.musicbtn}
          />
        </Button>
      ) : isRecording ? (
        <Button>
          <PauseIcon
            className={classes.musicbtn}
            onClick={() => setisRecording((prev) => !prev)}
          />
        </Button>
      ) : (
        <Button>
          <FiberManualRecordIcon
            className={classes.musicbtn}
            onClick={() => setisRecording((prev) => !prev)}
          />
        </Button>
      )}

      <Button>
        <StopIcon
          onClick={() => setstopRecording(true)}
          className={classes.musicbtn}
        />
      </Button>
    </MyContainer>
  );
};

export default App;
